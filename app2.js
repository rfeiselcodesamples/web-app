// clubs (♣), diamonds (♦), hearts (♥) and spades (♠)

var cardSymbols = ['clubs', 'diamonds', 'hearts', 'spades'];

var cardIndex = ['A', 'K', 'Q', 'J', '10', '9', '8', '7', '6', '5', '4', '3', '2'];

var node = document.createElement("div");

cardSymbols.forEach(function(symbol) {
  cardIndex.forEach(function(index) {
    generateCard(symbol, index)
  })
})

function generateCard(symbol, index) {
  //create card node
  var cardnode = document.createElement("div");
  cardnode.className = 'card';

  //apply correct color treatment
  if (symbol === 'diamonds' || symbol === 'hearts') {
    cardnode.style.color = 'red';
  }

  //create card children elements
  var divnode_upper = document.createElement("div");
  var divnode_lower = document.createElement("div");
  var divnode_center = document.createElement("div");

  //label card cormers
  divnode_upper.innerHTML = divnode_lower.innerHTML = index;

  //style card elements
  divnode_upper.className = "upper-left letter " + symbol;
  divnode_lower.className = "lower-right letter " + symbol;
  divnode_center.className = "center-center " + symbol;

  //create virtual DOM
  cardnode.appendChild(divnode_upper);
  cardnode.appendChild(divnode_center);
  cardnode.appendChild(divnode_lower);

  //attach to DOM
  document.body.appendChild(cardnode);
}